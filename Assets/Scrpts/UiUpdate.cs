﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiUpdate : MonoBehaviour
{


public TextMeshProUGUI PopulationCount;
public TextMeshProUGUI Resource1;
public TextMeshProUGUI Resource2;
public TextMeshProUGUI Resource3;



    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdatePopulation(int current, int max) {
        
        if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            PopulationCount.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }

        Debug.Log("Print red");
        PopulationCount.text = $"{current} / {max}";
    }

     public void UpdateResource1(int current, int max) {

        if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            Resource1.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }    

        Resource1.text = $"{current} / {max}";
    }

     public void UpdateResource2(int current, int max) {

       if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            Resource2.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }   

        Resource2.text = $"{current} / {max}";
    }

     public void UpdateResource3(int current, int max) {

        if(max - current > 5) { //Are we far below the max/recommended? If so, we'll turn the 'current' text red
            Resource3.text = $"<color=\"red\">{current}<color=\"white\"> / {max}";
            return;
        }

        Resource3.text = $"{current} / {max}";
    }
    

}
